describe('angularjs homepage patient list', function() {
  it('should view a patient in detail view and check name', function() {
    //browser.get('http://localhost:5757/app/#/');
    browser.get('https://lectures.fomalhaut.uberspace.de/latido-case/app/#/')

    var patientList = element.all(by.repeater('patient in patientList.patients'));

    //check if all 3 patients are there
    expect(patientList.count()).toEqual(3);
   
    //select the third patient
    patientList.get(2).element(by.css('a')).click();

    var patient = element(by.css('h3'));
    // check if name is correct (ergo patientlist is also alphabetically sorted)
    expect (patient.getText()).toBe('Schiller, Friedrich');
  });
});