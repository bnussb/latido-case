angular.module('patientApp', ['ngRoute','chart.js'])


  .config(function($routeProvider) {
   
    $routeProvider
      .when('/', {
        controller:'PatientListController',
        templateUrl:'list.html'
      })
      .when('/details/:patientId', {
        controller:'PatientListController',
        templateUrl:'detail.html'
      })
      .otherwise({
        redirectTo:'/'
      });
  })



  .controller('PatientListController', function($routeParams, $scope) {
    
    var patientList = this;
    patientList.patients = [];

    patientList.addPatient = function(patientId, patientName, patientCharts) {
      var bloodPressureChart = patientCharts[0]; ;
      var bodySurfaceChart = patientCharts[1];
      var cholesterolChart = patientCharts[2];

      patientList.patients.push({id:patientId, name:patientName, charts:patientCharts, lastMeasurements:[bodySurfaceChart.data[0].last(),bloodPressureChart.data[0].last()+"/"+bloodPressureChart.data[1].last(),cholesterolChart.data[0].last()]});
    };

    //static initialisation
    var patientCharts = getPatientsCharts();
    patientList.addPatient(1,"Bach, Johann", patientCharts);
    patientList.addPatient(2,"Mozart, Wolfgang", patientCharts);
    patientList.addPatient(3,"Schiller, Friedrich", patientCharts);
    //

    if ($routeParams.patientId){
      patientList.currentPatient= patientList.patients[$routeParams.patientId-1];
      console.log(patientList.currentPatient);
    }

  });

// Helper Functions
Array.prototype.last = function() {
    return this[this.length-1];
};

function getPatientsCharts(){
  var patientCharts = new Array();   
  var bloodPressureChart = getBloodPressureChartFromData([[135, 132, 139, 142, 140, 138, 144, 137, 136, 138, 135, 134],
      [88, 87, 89, 90, 92, 89, 91, 91, 88, 87, 86, 86]]);
  patientCharts.push(bloodPressureChart);

  var bodySurfaceChart = getBodySurfaceChartFromData([
      [1.87, 1.80, 1.76, 1.78, 1.80, 1.82, 1.85, 1.83, 1.86, 1.83, 1.80, 1.78]
  ]);

  patientCharts.push(bodySurfaceChart);


  var cholesterolChart = getCholesterolChartFromData([
    [87.88, 85.50, 82.63, 83.80, 99.24, 81.93, 81.07, 94.86, 85.15, 96.16, 99.31, 80.59]
  ]);

  patientCharts.push(cholesterolChart);

  return patientCharts;
}

function getBloodPressureChartFromData(data){
  var bloodPressureChart = new Array();
  bloodPressureChart.name = "Blutdruck";
  bloodPressureChart.labels = ['Jan', 'Feb', 'Mar', 'Apr', 'Mai', 'Jun', 'Jul', 'Aug','Sep','Okt','Nov','Dez'];
  bloodPressureChart.series = ['Systolisch', 'Diastolisch'];
  bloodPressureChart.data = data;
  return bloodPressureChart;
}

function getBodySurfaceChartFromData(data){
  var bodySurfaceChart = new Array();
  bodySurfaceChart.name = "Körperoberfläche";
  bodySurfaceChart.labels = ['Jan', 'Feb', 'Mar', 'Apr', 'Mai', 'Jun', 'Jul', 'Aug','Sep','Okt','Nov','Dez'];
  bodySurfaceChart.series = ['m&sup2;'];
  bodySurfaceChart.data = data;
  return bodySurfaceChart;
}

function getCholesterolChartFromData(data){
  var cholesterolChart = new Array();
  cholesterolChart.name = "Cholesterin";
  cholesterolChart.labels = ['Jan', 'Feb', 'Mar', 'Apr', 'Mai', 'Jun', 'Jul', 'Aug','Sep','Okt','Nov','Dez'];
  cholesterolChart.series = ['mg/dl'];
  cholesterolChart.data = data;
  return cholesterolChart;
}



